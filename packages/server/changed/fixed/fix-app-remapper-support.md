Resolve issue where the app name and organization id are undefined in the URL subdomain when using
the app remapper URL option.
